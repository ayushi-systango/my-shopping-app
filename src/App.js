import React,{Component} from 'react';
import {Switch, Route,BrowserRouter as Router} from 'react-router-dom';


import Navbar from './Components/UI/Navbar';
import ProductList from './Components/Products/ProductList';
import Details from './Components/Details/Details';
import Cart from './Components/Cart/Cart';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component{
	render(){
		return (
            <React.Fragment>
               <Navbar/>
               <Switch>
               <Route exact path="/" component={ProductList}></Route>
               <Route path="/details" component={Details}></Route>
                <Route path="/cart" component={Cart}></Route>
               </Switch>
            </React.Fragment>
            );
    }
 }

export default App;
