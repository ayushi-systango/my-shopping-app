import * as actionTypes from './actionTypes';

export const setModal = ( selectRow) => {
  return {
    type: actionTypes.SET_MODAL,
    selectRow:selectRow
  };
};

export const setModalOpenSuccess = error => {
  return {
    type: actionTypes.SET_MODAL_OPEN_SUCCESS
  };
};

export const setModalOpenFail =() =>
{
	return{
		type : actionTypes.SET_MODAL_OPEN_FAIL
	};
};

export const setModalCloseSuccess =() =>
{
  return{
    type : actionTypes.SET_MODAL_CLOSE_SUCCESS
  };
};

export const setModalCloseFail =() =>
{
  return{
    type : actionTypes.SET_MODAL_CLOSE_FAIL
  };
};




export const getSelectRow = (data,data1) => {
  console.log(data1.original);	
  return dispatch => {
    dispatch(setModalOpenSuccess());
    dispatch(setModal(data1.original));
  }
};


