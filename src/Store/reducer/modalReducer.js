import * as actionTypes from '../action/actionTypes';

const initialState = {
   isModelOpen:false,
   selectRow: null,
   error:null

};

const modalReducer = (state = initialState, action) => {
	switch (action.type) 
	{
	case actionTypes.SET_MODAL:
	return{
			...state,
			posts:action.posts,
      selectRow:action.selectRow
	}
    case actionTypes.SET_MODAL_OPEN_SUCCESS:
      return {
        ...state,
        isModelOpen:true,
        error:null
      };
    case actionTypes.SET_MODAL_OPEN_FAIL:
    console.log(action.pages);
    return{
    	...state,
      error:action.error

    };
    case actionTypes.SET_MODAL_CLOSE_SUCCESS:
    return{
      ...state,
      isModelOpen:false,
      selectRow:null

    };
    case actionTypes.SET_MODAL_CLOSE_FAIL:
    return{
      ...state,
      error:action.error

    };
    default:
      return state;
  }
};
export default modalReducer;