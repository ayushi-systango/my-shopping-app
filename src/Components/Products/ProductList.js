import React, {Component} from 'react';

import Products from './Products';

class ProductList extends Component{
	state={
		products:[]
	}
	render(){
		return (
            <React.Fragment>
            <div className="py-5">
            <div className="container">
            <div className="row">
            <Products/> 
            </div>
            </div>
            </div>
            </React.Fragment>
            );
    }
 }

export default ProductList;
