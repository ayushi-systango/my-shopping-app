import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {Button} from './Button';
import styled from 'styled-components';
//import 'bootstrap/dist/css/bootstrap.min.css';

class Navbar extends Component{
	render(){
		return (
			<NavWrapper className="navbar navbar-expand-sm  navbar-dark px-sm-5">

			<Link to='/'>
			// here we have to put logo image imageee
			</Link>
			<ul className="navbar-nav align-items-center">
			<li className="nav-item ml-5">
			<Link to="/" className="nav-link">
              Products
			</Link>
			</li>
			</ul>
            <Link to='/cart' className="ml-auto">
            <Button>
            <span className="mr-2">
             <i className="fas fa-cart-plus" />
            </span>
               My Cart
            </Button>
            </Link>
			</NavWrapper>
            );
    }
 }

 const NavWrapper=styled.nav`
background:var(--mainBlue);
.nav-link
{
	color:var(--mainWhite) !important;
	font-size:1.3rem;
	text-transform:capitalize;
}
 `;


export default Navbar;
