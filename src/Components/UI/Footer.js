import React from 'react';
import { Navbar , Container} from 'react-bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

const Footer =()=> {
    return(
      <Container>
      <Navbar bg="light" expand="lg" className="justify-content-center" >
      <Navbar.Text className="text-info justify-content-center">
      © 2020 Copyright :-  <a href="https://www.systango.com/">SYSTANGO</a>
      </Navbar.Text>
      </Navbar>
      </Container>
      )
  }

  export default Footer;
